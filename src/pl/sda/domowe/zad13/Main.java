package pl.sda.domowe.zad13;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj N:");
        int N = scanner.nextInt();

        Random generator = new Random();

        // A
        for (int i = 0; i < N; i++) {
            int b = generator.nextInt();
            System.out.println(b);
        }

        // B
        for (int i = 0; i < N; i++) {
            double b = generator.nextDouble();
            System.out.println(b);
        }

        // C
        for (int i = 0; i < N; i++) {
            boolean b = generator.nextBoolean();
            System.out.println(b);
        }

        // D
        int poczatekZakresu, koniecZakresu;
        System.out.println("Podaj poczatek zakresu:");
        poczatekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        koniecZakresu = scanner.nextInt();

        for (int i = 0; i < N; i++) {
            int b = generator.nextInt(koniecZakresu - poczatekZakresu + 1) + poczatekZakresu;
            System.out.println(b);
        }

        // E
        for (int i = 0; i < N; i++) {
            double b = generator.nextDouble() * (koniecZakresu - poczatekZakresu + 1) + poczatekZakresu;
            System.out.println(b);
        }
    }
}
