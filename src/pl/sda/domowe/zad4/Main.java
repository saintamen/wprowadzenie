package pl.sda.domowe.zad4;

public class Main {
    public static void main(String[] args) {

        int a = 1, b = 2, c = 3;
        int tymczasowa = a;
        a = b; // straciłem wartość a
        b = c;
        c = tymczasowa;
        System.out.println(a + "  " + b + "  " + c + "  ");
//
    }
}
