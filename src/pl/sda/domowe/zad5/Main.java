package pl.sda.domowe.zad5;

public class Main {
    public static int zmiennaStatycznaTest = 5;

    public static void main(String[] args) {
        boolean jest_cieplo = true;
        boolean wieje_wiatr = false;
        boolean swieci_slonce = false;

        boolean ubieram_sie_cieplo = !jest_cieplo || wieje_wiatr;
        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;
        boolean ubieram_kurtke = wieje_wiatr && !swieci_slonce && !jest_cieplo;

        if(ubieram_kurtke){
            System.out.println("Ubieram kurtke");
        }

        if(biore_parasol){
            System.out.println("Biorę parasol");
        }

        if(ubieram_sie_cieplo){
            System.out.println("Ubieram się ciepło");
        }
    }
}
