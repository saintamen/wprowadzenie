package pl.sda.domowe.zad6;

public class Main {
    public static void main(String[] args) {
        int waga = 100;
        int wzrost = 170;
        int wiek = 20;

        if (waga < 180 &&
                wzrost > 150 &&
                wiek > 10 &&
                wiek < 80 &&
                wzrost < 220) {
            System.out.println("Możesz wejść na kolejkę");
        } else {
            if (waga > 180) {
                System.out.println("Schudnij!");
            }
            if (wzrost < 150) {
                System.out.println("Urośnij!");
            }
            if (wiek < 10) {
                System.out.println("Idź po rodziców");
            }
            if (wiek > 80) {
                System.out.println("Za późno!");
            }
            if (wzrost > 220) {
                System.out.println("Lipa...");
            }
            // forma wykluczająca kolejne warunki
//            if (waga > 180) {
//                System.out.println("Schudnij!");
//            } else if (wzrost < 150) {
//                System.out.println("Urośnij!");
//            } else if (wiek < 10) {
//                System.out.println("Idź po rodziców");
//            } else if (wiek > 80) {
//                System.out.println("Za późno!");
//            } else if (wzrost > 220) {
//                System.out.println("Lipa...");
//            }
            if (waga > 180) {
                System.out.println("Schudnij!");
            } else if (wzrost < 150) {
                System.out.println("Urośnij!");
            } else if (wiek < 10) {
                System.out.println("Idź po rodziców");
            } else if (wiek > 80) {
                System.out.println("Za późno!");
            } else if (wzrost > 220) {
                System.out.println("Lipa...");
            }
            System.out.println("Nie możesz wejść na kolejkę");
        }


    }
}
