package pl.sda.domowe.zad6_zbior3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int liczba;
        do {
            System.out.println("Podaj liczbę:");
            liczba = scanner.nextInt();

        } while (liczba < 0);


        // 2 ^ 0
        int wypisywana = 1;

        do {
            System.out.println(wypisywana);
            wypisywana *= 2;
        } while (wypisywana < liczba);


    }
}
