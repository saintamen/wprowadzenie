package pl.sda.wprowadzenie.Przyklady;

public class PetlaForNaWhile {
    public static void main(String[] args) {
        for (int iterator = 0; iterator < 100; iterator++) {
            System.out.println("Hello!");
        }


        // przełożone na while

        int iterator = 0;

        while (iterator < 100) {
            // kod pętli
            System.out.println("Hello!");

            iterator++;
        }

        /// Przykład E z zadań
        int iter = 30;
        while (iter <= 300) {
            if (iter % 3 == 0 && iter % 5 == 0) {
                System.out.println("Podzielna przez 3 i 5:" + iter);
            }

            // inkrementacja
            iter++;
        }

    }
}
