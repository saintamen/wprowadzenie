package pl.sda.wprowadzenie.Przyklady;

import java.util.Scanner;

public class PetlaZDzielnikiem {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj dzielnik:");
        int dzielnik = scanner.nextInt();

        System.out.println("Podaj liczbe do ktorej iterujemy:");
        int liczba = scanner.nextInt();

        for (int i = 0; i < liczba; i++) {
            if (i % dzielnik == 0) {
                System.out.println("Podzielna przez " + dzielnik + " = " + i);
            }
        }

        for (int i = 0; i < 100; i++) {
            if (i % 2 != 0) {
                System.out.print(i + ", ");
            }
        }

        char a = 65;
//        int a = 'A';

        System.out.println(a);
    }
}
