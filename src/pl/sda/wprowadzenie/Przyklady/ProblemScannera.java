package pl.sda.wprowadzenie.Przyklady;

import java.util.Scanner;

public class ProblemScannera {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String linia = scanner.nextLine();
        System.out.println(linia);

        String slowo = scanner.next();
        System.out.println(slowo);

        String drugaLinia = scanner.nextLine();
        System.out.println(drugaLinia);

        int wartosc = scanner.nextInt();
        System.out.println(wartosc);

        String trzeciaLinia = scanner.nextLine();
        System.out.println(trzeciaLinia);

        int zamienionaTrzeciaLinia = Integer.parseInt(trzeciaLinia);
        double zamienionaTrzeciaLiniaNaDouble = Double.parseDouble(trzeciaLinia);

        System.out.println(zamienionaTrzeciaLinia);
        System.out.println(zamienionaTrzeciaLiniaNaDouble);

    }
}
