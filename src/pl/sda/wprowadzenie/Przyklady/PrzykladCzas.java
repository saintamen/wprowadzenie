package pl.sda.wprowadzenie.Przyklady;

import java.sql.Timestamp;
import java.time.*;
import java.util.Date;

public class PrzykladCzas {
    public static void main(String[] args) {
        // stare
        Date date = new Date();

        // data
        LocalDate date1 = LocalDate.now();

        // czas
        LocalTime time1 = LocalTime.now();

        // data i czas
        LocalDateTime localDateTime1 = LocalDateTime.now();

        // różnica czasu - precyzyjna, jeśli interesuje mnie różnica podana w minutach/sekundach
        Duration duration = Duration.between(LocalDateTime.now(), localDateTime1);

        // różnica czasu - mniej precyzyjna - podaje różnicę czasu w miesiącach/latach/dniach
        Period period = Period.between(LocalDate.now(), date1);

        date1 = date1.plusDays(3);

        long timestamp = System.currentTimeMillis();
        long timestampNano = System.nanoTime();
        Timestamp timestamp2 = new Timestamp(timestamp);


        System.out.println("Lat: " + period.getYears() + " dni : " + period.getDays());
    }
}
