package pl.sda.wprowadzenie.Przyklady;

import java.util.Random;
import java.util.Scanner;

public class PrzykladDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //
        // Zaimplementuj grę w zgadywankę.
        // Wylosuj liczbę (użyj do tego klasy Random) z zakresu do 100.
        // Użytkownik ma podawać liczbę dopóki nie odgadnie wylosowanej liczby.
        //
        // Wykorzystaj pętlę do...while
        Random generator = new Random();
        int wygenerowana = generator.nextInt(100);


        int liczbaOdUzytkownika;
        do{
            System.out.println("Podaj liczb" +
                    "ę:");
            liczbaOdUzytkownika = scanner.nextInt();

        }while (liczbaOdUzytkownika != 0);
    }
}
