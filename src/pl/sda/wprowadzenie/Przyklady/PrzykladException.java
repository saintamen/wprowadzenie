package pl.sda.wprowadzenie.Przyklady;

public class PrzykladException {
    public static void main(String[] args) {
        int a = 5;
        int b = 0;
//
//        try {
//            System.out.println("Próba!");
            System.out.println(a / b);
//            System.out.println("Udalo sie !");
//        }catch (Exception wyjatek){
//            System.out.println("Błąd");
//
//        }
//        System.out.println("Wypis po catch");


        try {
            Student student = new Student();
            System.out.println(student.getName());
            System.out.println("OK student");


            System.out.println("Próba!");
            System.out.println(a / b);
            System.out.println("OK");
        }catch (ArithmeticException e){
            System.out.println("Error arithmetic");
        }catch (NullPointerException npe){
            System.out.println("Error null");
        }catch (Exception e){
            System.out.println("Other type of error");
        }
    }
}
