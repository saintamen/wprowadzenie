package pl.sda.wprowadzenie.Przyklady;

public class PrzykladMetoda {
    public static void main(String[] args) {

        int[] tab = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};

        int wynik = sumujElementyTablicy(tab);
//        System.out.println(sumujElementyTablicy(tab));
        System.out.println(wynik);

        int[] tablica2 = new int[]{10, 11, 12, 13, 14, 15, 16, 18};
        int wynik2 = sumujElementyTablicy(tablica2);
        System.out.println(wynik2);
    }

    // DRY - Don't repeat yourself
    // KISS - Keep it simple stupid
    public static int sumujElementyTablicy(int[] tablica) {
        if (tablica.length > 0) {
            int suma = 0;
            for (int i = 0; i < tablica.length; i++) {
                suma += tablica[i];
            }

            return suma;
        } else {

        }


        // zwrócenie zera.
        return 0;
    }
}
