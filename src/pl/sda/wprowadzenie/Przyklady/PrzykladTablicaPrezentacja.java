package pl.sda.wprowadzenie.Przyklady;

public class PrzykladTablicaPrezentacja {
    public static void main(String[] args) {
        int[] tablica = new int[]{1,3,5,10};

        for (int i = 0; i < tablica.length; i++) {
            System.out.print("Indeks: " + i);
            System.out.print(" Wartość: " + tablica[i]);
            System.out.println();
        }
    }
}
