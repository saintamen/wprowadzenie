package pl.sda.wprowadzenie.Przyklady;

public class PrzykladyPetli {
    public static void main(String[] args) {

        // f)
        for (int i = -300; i < 300; i++) {
            if (i % 2 != 0) {
                System.out.print(i + ";");
            }
        }

        System.out.println();

        // g)
        for (int i = -100; i < 101; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ";");
            }
        }

        System.out.println();

        // l)
        for (int i = 1; i < 101; i++) {
            System.out.println(i + ". Hello World!");
        }
        for (int i = 0; i <= 100; i++) {
            System.out.println((i+1) + ". Hello World!");
        }


    }
}
