package pl.sda.wprowadzenie.Zad1Obiekty;

public class Laptop extends Komputer {
    private double wielkośćMatrycy;
    private boolean czyPosiadaRetine;

    public Laptop(int potrzebnaMoc, String producent, TypProcesora typ) {
        super(potrzebnaMoc, producent, typ);
    }

    public double getWielkośćMatrycy() {
        return wielkośćMatrycy;
    }

    public void setWielkośćMatrycy(double wielkośćMatrycy) {
        this.wielkośćMatrycy = wielkośćMatrycy;
    }

    public boolean isCzyPosiadaRetine() {
        return czyPosiadaRetine;
    }

    public void setCzyPosiadaRetine(boolean czyPosiadaRetine) {
        this.czyPosiadaRetine = czyPosiadaRetine;
    }
}
