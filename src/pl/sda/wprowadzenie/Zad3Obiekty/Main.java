package pl.sda.wprowadzenie.Zad3Obiekty;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Robot robot = new Robot(100, "Leszek", false);
        String komenda;
        do {
            komenda = scanner.next();
            if (komenda.equals("naladuj")) {
                robot.naładujRobota();
            } else if (komenda.equals("wylacz")) {
                robot.wyłącz();
            } else if (komenda.equals("wlacz")) {
                robot.włącz();
            } else if (komenda.equals("ruch")) {
                System.out.println("Jaki ruch wykonać?");
                komenda = scanner.next().toUpperCase();

                try {
                    RuchRobota ruchRobota = RuchRobota.valueOf(komenda);
                    robot.poruszRobotem(ruchRobota);
                }catch (IllegalArgumentException iae){
                    System.out.println("Nie ma takiej komendy!");
                }
            }
        } while (!komenda.equals("zamknij"));
    }
}
