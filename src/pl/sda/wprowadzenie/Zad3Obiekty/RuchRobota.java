package pl.sda.wprowadzenie.Zad3Obiekty;

public enum RuchRobota {
    KROK_LEWA(5),
    KROK_PRAWA(5),
    RUCH_REKA_LEWA(15),
    RUCH_REKA_PRAWA(15),
    SKOK(25);

    private int potrzebnaMoc;

    RuchRobota(int ileMocy) {
        this.potrzebnaMoc = ileMocy;
    }

    public int getPotrzebnaMoc() {
        return potrzebnaMoc;
    }
}
