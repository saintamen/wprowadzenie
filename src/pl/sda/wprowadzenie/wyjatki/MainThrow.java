package pl.sda.wprowadzenie.wyjatki;

public class MainThrow {

    
    private static void metoda4() throws RzucanyWynik {
//        throw new RuntimeException("blad cholera!");
        throw new RzucanyWynik(7);
    }

    public static void main(String[] args) {
        try {
            metoda1();
        } catch (Throwable e) {
            System.out.println("Przechwycony " + e.getMessage());
        }
    }

    private static void metoda1() throws RzucanyWynik {
        metoda2();
    }

    private static void metoda2() throws RzucanyWynik {
        metoda3();
    }


    private static void metoda3() throws RzucanyWynik {
        metoda4();
    }

}
