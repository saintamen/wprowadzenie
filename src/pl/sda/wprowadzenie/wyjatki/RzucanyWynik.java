package pl.sda.wprowadzenie.wyjatki;

public class RzucanyWynik extends Throwable {
    private int wynik;

    public RzucanyWynik(int wynik) {
        super("" + wynik);
        this.wynik = wynik;
    }

    public int getWynik() {
        return wynik;
    }

    public void setWynik(int wynik) {
        this.wynik = wynik;
    }
}
