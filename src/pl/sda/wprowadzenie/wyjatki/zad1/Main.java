package pl.sda.wprowadzenie.wyjatki.zad1;

public class Main {
    public static void main(String[] args) {
        metoda_sposob1(1, -1);

//        try {
//        metoda_sposob2(1, -1);
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Wystąpił błąd, treść błędu: " + e.getMessage());
//        }
    }

    private static void metoda_a(int a, int b) {
        if (b < 0) {
            System.out.println("B < 0");
        }
    }

    private static void metoda_sposob1(int a, int b) {
        try {
            if (b < 0) {
                throw new Exception("B < 0");
            }
        } catch (Exception e) {
            // co ma się stać jeśli błąd wystąpi
            System.out.println("Wystąpił błąd, treść błędu: " + e.getMessage());
        }
    }

    private static void metoda_sposob2(int a, int b) throws Exception {
        if (b < 0) {
            throw new Exception("B < 0");
        }
    }
}
