package pl.sda.wprowadzenie.zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {



        // przykład i / j
        Scanner scanner = new Scanner(System.in);
        String zmienna = "cos"; // predefiniowane miejsce w pamięci
        String zmienna2 = scanner.next(); // miejsce w pamieci z konsoli/klawiatury

        System.out.println(zmienna.equals("cos")); // porównanie treści
        System.out.println(zmienna == zmienna2); // porównanie referencji
    }
}
